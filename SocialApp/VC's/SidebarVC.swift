//
//  SidebarVC.swift
//  SocialApp
//
//  Created by Megha on 4/5/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class SidebarVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    // outlets

    @IBOutlet weak var tableView: UITableView!
    
    // data
    let segues = ["People in my Proximity", "News Feed", "TimeLine" , "Connections","Messages","Notifications","Media","Groups","Blogs","Events","Badges","Search","Manage Settings","Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewDelegate&DataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return segues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! SidebarCell
        cell.lblForDesc?.text = segues[(indexPath as NSIndexPath).row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nvc = self.mainNavigationController()
        if let hamburguerViewController = self.findHamburguerViewController() {
            hamburguerViewController.hideMenuViewControllerWithCompletion({ () -> Void in
                nvc.visibleViewController?.performSegue(withIdentifier: self.segues[(indexPath as NSIndexPath).row], sender: nil)
                hamburguerViewController.contentViewController = nvc
            })
        }
    }
    
    // MARK: - Navigation
    
    func mainNavigationController() -> DLHamburguerNavigationController {
        return self.storyboard?.instantiateViewController(withIdentifier: "DLDemoNavigationViewController") as! DLHamburguerNavigationController
    }
}
