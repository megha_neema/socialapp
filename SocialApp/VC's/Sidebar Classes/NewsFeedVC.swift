//
//  NewsFeedVC.swift
//  SocialApp
//
//  Created by Megha on 4/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit
import SwiftyJSON

class NewsFeedVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var objUtility = Utility()
    var cell = NewsFeedCell()
    var arrForNewsFeed = NSMutableArray()
    var selectedDataDict = NSMutableDictionary()

    @IBOutlet weak var viewForDesc: CardView!
    @IBOutlet weak var imageforUser: UIImageView!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageforUser.layer.cornerRadius = imageforUser.frame.size.width/2;
        imageforUser.clipsToBounds = true
        imageforUser.layer.borderWidth = 1.0;
        imageforUser.layer.borderColor = UIColor.gray.cgColor
        
        if (objUtility.getAPIMode().caseInsensitiveCompare("offline") == ComparisonResult.orderedSame){
            fetchdataFromFile()
        }else {
            print("fetch data with API")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.blue
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchdataFromFile() {
        let path = Bundle.main.path(forResource: "NewsFeed", ofType: "txt")
        let url = URL(fileURLWithPath: path!)
        do {
            let data = try Data(contentsOf: url)
            let json = try  JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSMutableDictionary
            arrForNewsFeed = json.value(forKey: "data") as! NSMutableArray
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }catch (let error) {
            print(error)
        }
    }
    
    // MARK: UITableViewDelegate&DataSource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedDataDict.allKeys.count > 0 {
            return 1
        } else {
            return arrForNewsFeed.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! NewsFeedCell
        
        if selectedDataDict.allKeys.count > 0 {
            cell.imageForProfile?.layer.cornerRadius = (cell.imageForProfile?.frame.size.width)! / 2;
            cell.imageForProfile?.clipsToBounds = true
            cell.imageForProfile?.layer.borderWidth = 1.0;
            cell.imageForProfile?.layer.borderColor = UIColor.gray.cgColor
            
            cell.lblForDesc?.text = selectedDataDict.value(forKey: "created_on") as? String
            
            cell.lblForProfileName?.text = selectedDataDict.value(forKey: "title") as? String
            cell.lblForLike?.text = selectedDataDict.value(forKey: "total_likes") as? String
            cell.lblForComment?.text = selectedDataDict.value(forKey: "total_comments") as? String
            
            cell.lblForPreviewDesc?.text = (selectedDataDict.value(forKey: "badge_data") as? NSDictionary)?.value(forKey: "reason") as? String
            
            let urlString = selectedDataDict.value(forKey: "user_name_photo") as? String
            
            URLSession.shared.dataTask(with: NSURL(string: urlString!)! as URL, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    print(error?.localizedDescription as Any)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    self.cell.imageForProfile?.image = image
                })
                
            }).resume()
            
            let urlString1 = (selectedDataDict.value(forKey: "badge_data") as? NSDictionary)?.value(forKey: "badgephoto") as? String
            
            URLSession.shared.dataTask(with: NSURL(string: urlString1!)! as URL, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    print(error?.localizedDescription as Any)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    self.cell.imageForPreview?.image = image
                })
                
            }).resume()
            
            let urlString2 = (((selectedDataDict.value(forKey: "group_photo_detail"))! as! NSMutableArray).object(at: 0) as AnyObject).value(forKey: "photoPicPath") as? String
            
            URLSession.shared.dataTask(with: NSURL(string: urlString2!)! as URL, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    print(error?.localizedDescription as Any)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    self.cell.imageForPost?.image = image
                })
                
            }).resume()
            
            return cell
        }
        
        cell.imageForProfile?.layer.cornerRadius = (cell.imageForProfile?.frame.size.width)! / 2;
        cell.imageForProfile?.clipsToBounds = true
        cell.imageForProfile?.layer.borderWidth = 1.0;
        cell.imageForProfile?.layer.borderColor = UIColor.gray.cgColor
        cell.btnForViewDesc?.tag = indexPath.row

        cell.lblForDesc?.text = (arrForNewsFeed.object(at: indexPath.row) as AnyObject).value(forKey: "created_on") as? String
        
        cell.lblForProfileName?.text = (arrForNewsFeed.object(at: indexPath.row) as AnyObject).value(forKey: "title") as? String
        cell.lblForLike?.text = (arrForNewsFeed.object(at: indexPath.row) as AnyObject).value(forKey: "total_likes") as? String
        cell.lblForComment?.text = (arrForNewsFeed.object(at: indexPath.row) as AnyObject).value(forKey: "total_comments") as? String
        
        cell.lblForPreviewDesc?.text = ((arrForNewsFeed.object(at: indexPath.row) as AnyObject).value(forKey: "badge_data") as? NSDictionary)?.value(forKey: "reason") as? String
        
        let urlString = (arrForNewsFeed.object(at: indexPath.row) as AnyObject).value(forKey: "user_name_photo") as? String
        
        URLSession.shared.dataTask(with: NSURL(string: urlString!)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error?.localizedDescription as Any)
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.cell.imageForProfile?.image = image
            })
            
        }).resume()
        
        let urlString1 = ((arrForNewsFeed.object(at: indexPath.row) as AnyObject).value(forKey: "badge_data") as? NSDictionary)?.value(forKey: "badgephoto") as? String
            
        URLSession.shared.dataTask(with: NSURL(string: urlString1!)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error?.localizedDescription as Any)
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.cell.imageForPreview?.image = image
            })
            
        }).resume()
        
        let urlString2 = ((((arrForNewsFeed.object(at: indexPath.row) as AnyObject).value(forKey: "group_photo_detail"))! as! NSMutableArray).object(at: 0) as AnyObject).value(forKey: "photoPicPath") as? String
            
        URLSession.shared.dataTask(with: NSURL(string: urlString2!)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error?.localizedDescription as Any)
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.cell.imageForPost?.image = image
            })
            
        }).resume()
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    @IBAction func tapOnShareBtn(_ sender: UIButton) {
    }
    
    @IBAction func tapOnLikeBtn(_ sender: UIButton) {
    }
    
    @IBAction func tapOnCmntBtn(_ sender: UIButton) {
    }
    
    @IBAction func tapOnMenuBtn(_ sender: UIBarButtonItem) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    func tapOnBackBtn(sender: UITapGestureRecognizer) {
        self.navigationController?.navigationBar.topItem?.title = "News Feed"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"menu"), style: .plain, target: self, action: #selector(tapOnMenuBtn))
        selectedDataDict.removeAllObjects()
        viewForDesc?.isHidden = false
        fetchdataFromFile()
    }
    
    @IBAction func tapOnViewDesc(_ sender: UIButton) {
        createviewForDesc()
    }
    
    func createviewForDesc() {
        selectedDataDict = (arrForNewsFeed.object(at: (self.cell.btnForViewDesc?.tag)!) as AnyObject) as! NSMutableDictionary
        viewForDesc?.isHidden = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"arrow_back"), style: .plain, target: self, action: #selector(tapOnBackBtn))
        self.navigationController?.navigationBar.topItem?.title = "Posts"
        self.tableView.reloadData()
    }
    
    @IBAction func tapOnAddPost(_ sender: UIButton) {
        print("Add on Posts")
    }
    

}
