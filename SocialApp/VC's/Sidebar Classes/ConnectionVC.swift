//
//  ConnectionVC.swift
//  SocialApp
//
//  Created by Megha on 4/10/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class ConnectionVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
  
    let arrayData = ["Connections", "Requests", "Suggestions" , "Invites"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
         tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.blue
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        super.viewDidAppear(animated)
    }
    
    @IBAction func tapOnMenuBtn(_ sender: UIBarButtonItem) {
        self.findHamburguerViewController()?.showMenuViewController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewDelegate&DataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Menu Cell", for: indexPath) as! ConnectionCell
        cell.lblForName?.text = arrayData[(indexPath as NSIndexPath).row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            let obj = storyboard!.instantiateViewController(withIdentifier: "ConnectionDetailVC") as! ConnectionDetailVC
            self.navigationController?.pushViewController(obj, animated: true)
        default:
            break
        }
        
    }
}
