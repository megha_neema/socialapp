//
//  ConnectionCell.swift
//  SocialApp
//
//  Created by Megha on 4/10/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class ConnectionCell: UITableViewCell {

    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var imageForName: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
