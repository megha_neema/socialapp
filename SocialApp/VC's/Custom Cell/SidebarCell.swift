//
//  SidebarCell.swift
//  SocialApp
//
//  Created by Megha on 4/5/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class SidebarCell: UITableViewCell {


    @IBOutlet weak var imageForSidebar: UIImageView!
    
    @IBOutlet weak var lblForDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
