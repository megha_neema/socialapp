//
//  NewsFeedCell.swift
//  SocialApp
//
//  Created by Megha on 4/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class NewsFeedCell: UITableViewCell {
    
    
    @IBOutlet weak var lblForProfileName: UILabel!
    @IBOutlet weak var lblForDesc: UILabel!
    @IBOutlet weak var lblForPreviewDesc: UILabel!
    @IBOutlet weak var imageForPreview: UIImageView!
    @IBOutlet weak var imageForProfile: UIImageView!
    @IBOutlet weak var lblForComment: UILabel!
    @IBOutlet weak var lblForLike: UILabel!
    @IBOutlet weak var imageForPost: UIImageView!
        @IBOutlet weak var btnForViewDesc: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
