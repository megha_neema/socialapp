//
//  FilterCell.swift
//  SocialApp
//
//  Created by Megha on 4/5/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet weak var imageForProfile: UIImageView!
    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var lblForDesc: UILabel!
    @IBOutlet weak var btnForCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
