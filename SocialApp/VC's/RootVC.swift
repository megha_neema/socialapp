//
//  RootVC.swift
//  SocialApp
//
//  Created by Megha on 4/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class RootVC: DLHamburguerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func awakeFromNib() {
        self.contentViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC")
        self.menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "SidebarVC")
    }
}



