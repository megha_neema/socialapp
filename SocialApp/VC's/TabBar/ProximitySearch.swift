//
//  ProximitySearch.swift
//  SocialApp
//
//  Created by Megha on 4/5/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class ProximitySearch: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var indexOfSelection : IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.lightGray
        self.tableView.backgroundColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func actionForSideMenu(_ sender: Any) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    @IBAction func actionForSearch(_ sender: Any) {
    }
    
    
    @IBAction func actionForGridList(_ sender: Any) {
    }
    
    
    @IBAction func actionOnCheckBtn(_ sender: UIButton) {
        if (sender.isSelected) {
            sender.isSelected = false
        }else {
            sender.isSelected = true
        }
    }
    
    // MARK: UITableViewDelegate&DataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! FilterCell
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.lightGray
        cell.lblForName?.text = "Adam Selfridge"
        cell.lblForDesc?.text = "0.000189870593201 Miles away"
        cell.imageForProfile?.image = UIImage(named: "userPhoto")
        cell.btnForCheck?.setImage(UIImage(named: "unchecked"), for: .normal)
        cell.btnForCheck?.setImage(UIImage(named: "checked"), for: .selected)
        cell.btnForCheck?.isSelected = false
        return cell
    }
    
}
