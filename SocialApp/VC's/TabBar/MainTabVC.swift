//
//  MainTabVC.swift
//  SocialApp
//
//  Created by Megha on 4/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class MainTabVC: UITabBarController {

    //@IBOutlet weak var tabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.barTintColor = .blue
        tabBar.tintColor = .white
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor.white, size: CGSize(width:tabBar.frame.width/CGFloat(tabBar.items!.count), height:tabBar.frame.height), lineWidth: 3.0)
        
        self.navigationController?.isNavigationBarHidden = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x:0, y:size.height - lineWidth, width:size.width, height:lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
