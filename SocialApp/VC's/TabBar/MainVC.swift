//
//  MainVC.swift
//  SocialApp
//
//  Created by Megha on 3/30/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var lblForDesc: UILabel!
    @IBOutlet weak var imageForSocial: UIImageView!
    
    var imageList: NSArray = ["pic1", "pic2", "pic3"]
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblForName.text = "Adam Selfridge"
        lblForDesc.text = "0.000189870593201 Miles away"
        addGesturetoImageView()
        changeImage(0);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Mark :- Methods for Image Swipe
    func addGesturetoImageView(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(MainVC.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(MainVC.respondToSwipeGesture(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if(currentIndex > 0) {
                    currentIndex -= 1
                    showAminationOnAdvert(kCATransitionFromLeft);
                }
               changeImage(currentIndex);
                
            case UISwipeGestureRecognizerDirection.left:
                if(currentIndex < self.imageList.count-1) {
                    currentIndex += 1
                    showAminationOnAdvert(kCATransitionFromRight);
                }
                changeImage(currentIndex);
                
            default:
                break
            }
        }
    }
   
    func showAminationOnAdvert(_ subtype :String){
        let  transitionAnimation = CATransition();
        transitionAnimation.type = kCATransitionPush;
        transitionAnimation.subtype = subtype;
        
        transitionAnimation.duration = 0.5;
        
        
        transitionAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut);
        transitionAnimation.fillMode = kCAFillModeBoth;
        
        imageForSocial.layer.add(transitionAnimation, forKey: "fadeAnimation")
        
    }
    
    func changeImage(_ index:Int){

        let imageName = self.imageList[index];
        self.imageForSocial.image = UIImage(named: imageName as! String)
    }

    //Mark :- Method for Sidebar and grid view
    @IBAction func actionForSideBar(_ sender: Any) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
}

