//
//  Utility.swift
//  SocialApp
//
//  Created by Megha on 4/7/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class Utility: NSObject {

    func getAPIMode() -> String {
        let mainBundle = Bundle.main
        let value: String = mainBundle.object(forInfoDictionaryKey: "API_Mode") as! String
        return value
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
